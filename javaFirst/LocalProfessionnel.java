package exo2;

public class LocalProfessionnel extends Habitation {
	private int nbEmployes;
	private static double TauxSalaries = 150.0;

	public LocalProfessionnel(String occupant, String adresse,
			double surfaceHabitable, int nbEmployes) {
		super(occupant, adresse, surfaceHabitable);
		this.nbEmployes = nbEmployes;
	}

	@Override
	public double calculerTaxe() {
	
		return (super.calculerTaxe()+TauxSalaries+TauxSalaries*((nbEmployes-1)/10));
	}

	@Override
	public void afficherInfos() {
		
		super.afficherInfos();
		System.out.println("Nombre d'employ�s : " + nbEmployes);
	}
}
