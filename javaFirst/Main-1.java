package exo1;
import tpjava.chenille.Chenille;
public class Main {

	public static void main(String[] args) {
		ChenilleSonore chenille = new ChenilleSonore();
		
		chenille.afficher();
		chenille.setTaille(4);
		chenille.afficher();
		chenille.setTaille(8);
		chenille.afficher();
		
		chenille.grandir();
		chenille.setDeplacement(5);
		chenille.afficher();
		chenille.rapetisser();
		chenille.setDeplacement(8);
		chenille.afficher();
		System.out.println(chenille.getTaille());
		
		

	}

}
