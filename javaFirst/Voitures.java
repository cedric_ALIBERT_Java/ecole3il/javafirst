package exo1;

public class Voitures extends Vehicules {
	
	private int nbPlaces;
	public Voitures(String M, double l, int p, int nbPlaces) {
		super(M, l, p);
		this.nbPlaces = nbPlaces;
	}
	public void ChangeNbPlaces(int nbp)
	{
		nbPlaces = nbp;
		setDate();
	}
	public void getInfosSpecifiques()
	{
		super.getInfosSpecifiques();
		System.out.println(nbPlaces + "places assises");
	}

	
	
}
