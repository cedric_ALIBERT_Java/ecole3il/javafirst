package tpjava.chenille;

public class Chenille {
	
	// Constantes de classe
	private static final char TETE = '\u263A';
	private static final char ANNEAU = '\u25CB';
	
	
	// Variables d'instance protected
	protected int			taille;

	// Variables d'instance private
	private StringBuilder	forme;
	
	
	
	// Accesseurs et mutateurs (getters et setters)

	public void setTaille( int taille ) {
		
		if ( taille >= 0 ) {
			this.taille = taille;
			forme.delete( 0, forme.length() );
			for( int i = 0; i < taille; i++ ) {
				forme.append( ANNEAU );
			}
			forme.append( TETE );
		}
		
	}

	
	// Constructeur

	public Chenille() {
		forme = new StringBuilder();
		setTaille( 0 );
	}
	
	
	// M�thodes d'instance puliques

	public void afficher() {
		System.out.println( forme );
		System.out.flush();
	}
	
	
	
}
