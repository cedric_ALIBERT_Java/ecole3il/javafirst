package exo2;

public class Habitation {
	protected String occupant;
	protected String adresse;
	protected double surfaceHabitable;
	protected static double TauxDeBase = 1.5;
	public Habitation(String occupant,String adresse,double surfaceHabitable)
	{
		this.occupant = occupant;
		this.adresse = adresse;
		this.surfaceHabitable = surfaceHabitable;
	}
	public double calculerTaxe()
	{
		return (surfaceHabitable * TauxDeBase);
	}
	public void afficherInfos()
	{
		System.out.println("Classe Java : "+this.getClass().getName());
		System.out.println("Occupant :" + occupant );
		System.out.println("Adresse :" + adresse );
		System.out.println("Surface Habitable :" + surfaceHabitable );
	}
	public void setTauxDeBase(double TDB)
	{
		TauxDeBase = TDB;
	}
}
