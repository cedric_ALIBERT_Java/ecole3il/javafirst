package exo1;

import java.util.Date;

public class Vehicules {
	private final String matricule;
	private double longueur;
	private int poids;
	private Date date;
	public Vehicules(String M,double l,int p)
	{
		matricule=M;
		longueur=l;
		poids=p;
		date=new Date();
	}
	public String getMatricule() {
		return matricule;
	}
	public double getLongueur() {
		return longueur;
	}
	public double getPoids() {
		return poids;
	}
	public Date getDate() {
		return date;
	}
	public void ChangeLongueur(double l)
	{
		longueur = l;
		date = new Date();
	}
	public void ChangePoids(int p)
	{
		poids = p;
		date = new Date();
	}
	public void getInfosSpecifiques()
	{
		System.out.println("matricule : " + matricule);
		System.out.println("longueur : " + longueur);
		System.out.println("poids : " + poids);
		System.out.println("date derniere modif : " + date);
	}
	protected void setDate()
	{
		date = new Date();
	}
	
	
	
}
