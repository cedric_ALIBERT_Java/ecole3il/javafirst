package exo2;

public class MaisonIndividuelle extends Habitation{
	private boolean piscine;
	private double surfaceJardin;
	private static double TauxJardin = 0.5;
	private static double TauxPiscine = 100.0;
	
	public MaisonIndividuelle(String occupant,String adresse,double surfaceHabitable,boolean piscine,double surfaceJardin)
	{
		super(occupant,adresse,surfaceHabitable);
		this.piscine = piscine;
		this.surfaceJardin = surfaceJardin;
	}
	public double calculerTaxe()
	{
		if(piscine == true)
		{
			return(surfaceJardin*TauxJardin+super.calculerTaxe()+TauxPiscine);
		}
		else
			return(surfaceJardin*TauxJardin+super.calculerTaxe());
	}
	public void afficherInfos()
	{
		super.afficherInfos();
		System.out.println("Surface du jardin : " + surfaceJardin);
		if(piscine)
		{
			System.out.println("Avec piscine : oui");
		}
		else
			System.out.println("Avec piscine : non");
		
	}
	
}
