package exo1;

import tpjava.chenille.Chenille;

public class ChenilleAmelioree extends Chenille {
	protected int deplacement = 0;
	public ChenilleAmelioree()
	{
		System.out.println("Je suis une chenille am�lior�e");
	}
	public int getTaille()
	{
		return taille;
	}
	public void grandir()
	{
		setTaille(taille+2);
	}
	public void rapetisser()
	{
		setTaille(taille-2);
	}
	public void setDeplacement(int x)
	{
		deplacement = x;
	}
	public void afficher()
	{
		for(int i=0;i<deplacement;i++){
			System.out.print(" ");
		}
		super.afficher();
	}
}
