package exo1;

public class LocomotiveElectriques extends Locomotives {
	public LocomotiveElectriques(String M, double l, int p, int puissance,
			int volts) {
		super(M, l, p, puissance);
		this.volts = volts;
	}

	private int volts;

	@Override
	public void getInfosSpecifiques() {
		System.out.println(super.puissance + "kW,moteur electrique," + volts +"Volts");
		super.getInfosSpecifiques();
	}
	public void changeVolts(int v)
	{
		volts = v;
	}
}
