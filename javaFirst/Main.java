package exo2;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		
		
		Habitation appartement = new Habitation("Jacques DUPONT","44 Rue Saite-Anne",82.5);
		appartement.afficherInfos();
		System.out.println("Taxe : " + appartement.calculerTaxe());
		System.out.println();
		
		appartement.setTauxDeBase(2.0);
		
		MaisonIndividuelle maison = new MaisonIndividuelle("L�a FOX","7 Rue Hugo",212.50,true,644.80);
		maison.afficherInfos();
		System.out.println("Taxe : " + maison.calculerTaxe());
		System.out.println();
		
		LocalProfessionnel local = new LocalProfessionnel("INFO PLUS", "22 RUE WILSON", 160.00, 9);
		local.afficherInfos();
		System.out.println("Taxe : " + local.calculerTaxe());
		System.out.println();
		
		Habitation uneHabitation;
		uneHabitation = appartement;
		uneHabitation.afficherInfos();
		System.out.println("Taxe : " + uneHabitation.calculerTaxe());
		System.out.println();
		
		uneHabitation = maison;
		uneHabitation.afficherInfos();
		System.out.println("Taxe : " + uneHabitation.calculerTaxe());
		System.out.println();
		
		uneHabitation = local;
		uneHabitation.afficherInfos();
		System.out.println("Taxe : " + uneHabitation.calculerTaxe());
		System.out.println();
		
		ArrayList<Habitation> habitations = new ArrayList<Habitation>();
		habitations.add(appartement);
		habitations.add(maison);
		habitations.add(local);
		
		for(Habitation h : habitations)
		{
			System.out.println();
			h.afficherInfos();
			System.out.println("Taxe : "+h.calculerTaxe()+"�");
		}
	}

}
