package exo1;

public class Locomotives extends Vehicules {
	protected int puissance;

	public Locomotives(String M, double l, int p, int puissance) {
		super(M, l, p);
		this.puissance = puissance;
	}

	public void changePuissance(int p)
	{
		puissance = p;
		setDate();
	}
	
}
