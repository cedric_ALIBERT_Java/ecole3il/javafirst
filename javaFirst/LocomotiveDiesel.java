package exo1;

public class LocomotiveDiesel extends Locomotives {
	public LocomotiveDiesel(String M, double l, int p, int puissance,
			int reservoire) {
		super(M, l, p, puissance);
		this.reservoire = reservoire;
	}

	private int reservoire;

	@Override
	public void getInfosSpecifiques() {
		System.out.println(super.puissance + "kW,moteur diesel,réservoire" + reservoire +"litre");
		super.getInfosSpecifiques();
	}
	public void changeReservoir(int r)
	{
		reservoire=r;
		setDate();
	}
	
}
